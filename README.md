## Writing articles

Articles go into `content/blog/*.md`.

Add a header on top:

```
+++
title = "Title goes here"
date = "2101-01-01"

[taxonomies]
tags=["CRT"]
+++
```

## Publishing checklist

- [ ] Run `zola serve` and check the article at <http://127.0.0.1:1111/>.
- [ ] Add a `<!-- more -->` tag and check the snippet in the homepage.
- [ ] Once you're done, fix the article's date before pushing.

## Preview

```sh
set BRANCH (git rev-parse --abbrev-ref HEAD)
git co staging-base && git rebase master &&\
    git co -B staging && git merge --no-edit $BRANCH && git submodule update --recursive &&\
    git push --force-with-lease --recurse-submodules=no;
git co $BRANCH
```

Then check <https://nyanpasu64-staging.gitlab.io>.

## Writing shortcodes

https://www.getzola.org/documentation/content/image-processing/

https://www.getzola.org/documentation/templates/overview/

https://tera.netlify.app/docs/#templates `{{ __tera_context }}` + https://jsonformatter.curiousconcept.com/ for debugging

```
{{ youtube(id="GoQ9q1lwKHI") }}
{{ img(src="name.png", alt="image", title="hi") }}
{{ thumb(src="stray2.jpg", alt="Photo of Stray played on a 480i Trinitron CRT.", title=`"hello world" ([source](...))`) }}
```

https://www.getzola.org/documentation/content/shortcodes/

> string: surrounded by double quotes, single quotes or backticks

You *must* use backticks to enclose a message with double quotes inside; single quotes do not work.

Note that relative links, including `thumb()`, will break if you run `zola serve`, open a page, then remove the trailing slash from the URL ([bug report](https://github.com/getzola/zola/issues/1781)). This is not a problem on actual GitLab Pages, which will add the slash to the URL if you visit a page without it.

.html shortcodes are embedded into the generated html, .md shortcodes act like macros and can break subsequent nested bullets. .html shortcodes escape `<a href="">` urls unlike .md, but the resulting HTML is parsed the same in browsers.

## Validating changes to shared templates

```sh
git co change~
zola build -o before
git co change
zola build -o after
kdiff3 before after
```

## Transparent images

https://github.com/getzola/zola/issues/2136
