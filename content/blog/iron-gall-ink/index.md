+++
title = "Making iron gall ink from oak galls"
date = 2024-06-11
+++

Back in May, I was taking a walk around the neighborhood when I came across an oak tree with dozens of galls hanging from the stem. At first I thought these were seed pods and didn't even recognize the tree as an oak, but PlantNet (and later Seek) informed me it was a valley oak. At this point I realized the pulpy brown masses hanging from the stems were oak galls, and I had the idea to make my own iron gall ink like people did in centuries past. <!-- more -->

## History of iron gall ink

Oak trees form galls (such as oak apples and marbles) when certain wasps deposit their eggs in oak trees (often in leaf buds). The larvae produce chemicals which turn the oak bud into a swollen ball filled with tannin-rich pulp, which protects the growing insect from predators like birds (not always successfully). After the insect emerges, the galls dry out into a spongy mass which hangs on the tree or falls to the ground.

To make ink, people would collect galls (before or after the insect exits), crush them into a fine powder, and soak it in solvents like water, beer, or wine to dissolve the tannins and convert it into gallic acid. Afterwards, people would combine the tannin liquid with an iron sulfate solution to create iron-tannin compounds with a blue-black color, which can be used as ink (usually with gum arabic added to make the liquid flow better). [Traveling Scriptorium has a page](https://travelingscriptorium.com/2013/03/21/iron-gall-ink/) with details on historical iron gall ink production.

Since the 4th century, iron gall ink has been used to prepare manuscripts with writing that could not be washed off the paper's surface ([source](https://www.amphilsoc.org/blog/ins-outs-iron-gall-ink)). Unfortunately, over centuries, this ink can degrade and corrode the parchment or paper it's applied to, because the reaction between gallic acid and iron sulfate results in sulfuric acid and leftover iron ions, which both damage organic compounds. Iron gall ink is no longer used in either fountain pens or roller-ball pens today, as it can corrode fountain pens made of most metals; I believe it was historically used in quill dip pens.

Nowadays conservators have developed multiple techniques to preserve and restore documents written using iron gall ink; [irongallink.org has a page](https://irongallink.org/conservation-current-methods.html) on how this is done.

## Producing my own ink

I picked several oak galls from the tree as well as the ground underneath, including one live gall still attached to a branch. It was fun reconnecting with nature and interacting with wild places again.

{{ thumb(src="PXL_20240528_231103958.jpg", alt="Photo of one fresh yellow oak gall, along with five dark-colored dried-up galls.") }}

There are various guides online on how to make oak gall ink, including the [British Library's video](https://www.youtube.com/watch?v=y7k4-wj8mZ8), [Craft Invaders](https://craftinvaders.co.uk/how-to-make-oak-gall-ink/), and [Instructables](https://www.instructables.com/Making-Iron-Gall-Ink/). I had only read the Instructables guide prior to starting my project, and what I did is *very much* not a guide on how to best prepare ink; I probably made mistakes left and right due to inadequate preparation, but at least I was having fun, right! If you want to follow along, checking these guides should help produce better results.

I broke up the dried galls into a plastic container of water, into which I later added some vinegar to help dissolve the tannins. I spent an hour or so breaking up the galls into smaller pieces. Ideally you would've crushed the galls eg. in a mortar and pestle prior to soaking, but I only had one such unit and it was reserved for food (and not moldy oak pulp). Worryingly, some of the galls released orange dust or even mold spores when I broke them. I put on a N95 mask and tried to soak and squeeze the galls in water prior to crushing, to minimize dust entering the air.

For the fresh gall, I cut it into pieces (there might've been a grub at the center? I couldn't tell), squeezed the pieces into the liquid, and threw away the solids. Perhaps you're supposed to wait for the gall to dry out before using it, I don't know.

Interestingly, I noticed my finger was already turning black where the flush cutters I was using to break apart the soaking galls touched my finger. This made me think that perhaps even metallic iron could be used to convert the tannins into black ink.

{{ thumb(src="PXL_20240529_011022945.jpg", alt="Photo of me holding flush cutters, index finger stained black next to the exposed steel of the cutters.") }}

Afterwards, I let this mixture soak for a day before straining and removing the solids. The resulting liquid water was moderately brown, much less concentrated than the British Library video; perhaps I could've gathered more galls or used less water, ground them finer, or let the galls soak for longer before dumping the solids. Traveling Scriptorium says that it's normal for mold to grow on the gall solution, but I would be terrified of exposing my house to large amounts of mold releasing toxins and spores.

At this point I added steel wool to a small amount of the vinegar-tannin solution. Unfortunately, the oak gall did not turn black quickly. After waiting overnight again, interestingly this solution *did* turn black, but was filled with strands of iron rather than forming a smooth uniform ink. I prepared a plastic straw nib pen and tried writing on a notebook using this solution.

{{ thumb(src="PXL_20240530_173411991.jpg", alt="A plastic tub of black iron-gall ink, along with handwriting on a lined notebook using this ink, showing pale blue-black lines and splotches of ink splatter.", title="Unfortunately my ink did not come out as dark as professional preparations.") }}

The next day my iron sulfate powder arrived. Dissolving this into the brown liquid produced a much more uniform black fluid, though still with some small particles at the bottom. (Perhaps you'll get smaller black particles if you dissolve the iron sulfate in water and mix the two solutions together?)

{{ thumb(src="PXL_20240601_004432804.jpg", alt=`A bag of iron sulfate, along with black ink and more writing prepared using it. The writing says "isn't it ironic?"`, title=`Ink prepared using iron sulfate powder has less sedimented particles than the batch made with rusting steel wool. "Isn't it ironic?" Pun intended.`) }}

One day later, my gum arabic powder arrived. I made the possible mistake of ordering a powder when the British Library video shows the gum packaged as solid chunks being dissolved into its own solution. Additionally I added the gum directly into the ink, and some of the powder would not dissolve because it would form clumps with gooey exteriors keeping water away from the white powdery interior. Unfortunately adding gum did not noticeably improve my writing experience, and when I tried to re-sharpen my plastic straw quill for better results, I could not get it to apply ink *at all* no matter what angles I tried shaping the tip into. I tried dipping the pen into plain water and writing on paper, but it performed just as poorly.

## Conclusion

Preparing my own iron gall ink was a fun experiment in nature exploration, chemistry, calligraphy, and feeding the tinkering spirit. If you want to try it out yourself, it's probably better to plan ahead to avoid the issues I experienced. For example, you could prepare a scale to weigh your dried galls, find a way to grind them finely, let them soak for more than a day, boil or ferment the galls for better extraction, or prepare separate iron sulfate and gum arabic solutions to mix with the tannin liquid. [Traveling Scriptorium](https://travelingscriptorium.com/2013/03/21/iron-gall-ink/) claims:

> The extraction of tannins from gall nuts can be done in several ways: mixing, macerating, cooking, or fermenting. The amount of tannins extracted will vary according to the method used. It is generally thought that mixing with water will yield fewer tannins than macerating, macerating less than cooking, and cooking less than fermenting.

You would probably also get better results if you wrote using a professional quill or bamboo pen rather than a plastic straw. In my case, I wasn't keen on learning to sanitize and prepare bird feathers, nor spending even more money on bamboo nibs for a casual project (I had already purchased iron sulfate and gum arabic).

I wonder if pecan liquid can also be used to prepare gall ink. Pecans have an unpleasant astringent brown coating, so I often prepare them by soaking them in hot water and microwaving the pecans until the water boils. Afterwards the nuts look lighter-colored and taste better, but the water turns brown and astringent. If this liquid has a high enough tannin concentration, perhaps it could be used for ink too? (Though maybe pecans just don't have enough tannins on their surface alone, compared with the entire gall.)
